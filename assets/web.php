<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::group(
    ['middleware' => ['auth', 'admin']],
    function () {

        Route::resources([
            'users' => 'Admin\UserController',
            'patients' => 'Admin\PatientController',
            'cms' => 'Admin\ContentController',
            'tables' => 'Admin\TableController',
            'versions' => 'Admin\VersionController',
            'notification' => 'Admin\NotificationController',
            'sub-admin' => 'Admin\SubAdminController',
            'agency-role' => 'Admin\AgenciesRoleController',
            'payment-setting' => 'Admin\PaymentSettingController',
            'payment-billing' => 'Admin\PaymentBillingController',
            'setting' => 'Admin\SettingController',
            'agencies' => 'Admin\AgenciesController',
            'hubs' => 'Admin\HubsController',
            'assessments' => 'Admin\AssessmentsController',
            'services' => 'Admin\ServiceController',
            'contactus' => 'Admin\ContactController',
            'facilitiesrequest' => 'Admin\FacilityRequestController',

        ]);

        Route::post('admin/assessment/active', 'Admin\AssessmentsController@activcassessment')->name('place_referral');
        Route::get('admin-search-invoice', 'Admin\PaymentBillingController@serachInvoice')->name('admin_search_invoice');

        Route::get('generate/pdf', 'Admin\PaymentBillingController@generateInvoice')->name('htmltopdfviewadmin');
        Route::post('marksettle', 'Admin\PaymentBillingController@marksettle')->name('marksettle');
        Route::post('recpayment', 'Admin\PaymentBillingController@recpayment')->name('recpayment');

        Route::post('payment-setting/add', 'Admin\PaymentSettingController@add')->name('payment-setting/add');
        Route::get('setting-update', 'Admin\SettingController@index')->name('update-setting');

        // ASSESSMENT STEPS
        Route::post('admin/schedule-assessment-submit', 'Admin\AssessmentsController@scheduleAssessmentSubmit')->name('admin_schedule_assessment');
        Route::get('admin/assessment/accept/{id}', 'Admin\AssessmentsController@acceptassessment')->name('adminacceptaccessment');
        Route::post('admin/assessment/active/{agencyid?}/{lastinsertid?}/{care_type?}/{finalized_rate?}', 'Admin\AssessmentsController@activcassessment')->name('admin_active_assessment');
        Route::post('admin/bill-info-submit', 'Admin\AssessmentsController@billInfoSubmit')->name('admin_bill');
        Route::post('admin/assessment/check-bill', 'Admin\AssessmentsController@checkBills')->name('admincheckbill');
        Route::get('admin/assessment/send-alert/{id}/{type}', 'Admin\AssessmentsController@sendalert')->name('adminSendAlert');

        // Change hours
        Route::post('assessment/change-hours-submit/{ref?}', 'Admin\AssessmentsController@change_hours')->name('admin_change');
        Route::get('agencies/{id}', 'Admin\AgenciesController@show')->name('show-agency');
        Route::get('agencies/create/{id}', 'Admin\AgenciesController@create')->name('create-agency');
        Route::get('hubs/create/{id}', 'Admin\HubsController@create')->name('create-hub');
        // Route::get('patients/create/{id}', 'Admin\UserController@create')->name('admin-patients-create');
        Route::get('users/patients/{id}', 'Admin\UserController@patients')->name('admin-patients');

        Route::get('user-account-setting/{id?}', array('uses' => 'Admin\UserController@userProfile'))->name('admin-user-account-settings');
        Route::get('admin-user-billing-info/{id?}', array('uses' => 'Admin\UserController@billingInfo'))->name('admin-user-billing-info');
        Route::post('admin-user-billing-info-submit/{id?}', array('uses' => 'Admin\UserController@saveBillingInfoFirstTime'))->name('admin-user-billing-info-submit');
        Route::get('admin-show-user-billing-info/{id?}', array('uses' => 'Admin\UserController@showBillingInfo'))->name('admin-show-user-billing-info');
        Route::get('admin-user-add-new-card/{id}', array('uses' => 'Admin\UserController@adminUserAddNewCard'))->name('admin-user-add-new-card');
        Route::post('admin-user-card-info-submit', array('uses' => 'Admin\UserController@saveBillingInfo'))->name('admin-user-card-info-submit');

        Route::post('admin-user-card-delete/{id}', array('uses' => 'Admin\UserController@deleteCard'))->name('admin-user-card-delete');
        Route::post('admin-set-default-card/{id}', array('uses' => 'Admin\UserController@setDefaultCard'))->name('admin-set-default-card');
        
        // Update the user details
        Route::post('user-account-submit', array('uses' => 'Admin\UserController@updateUserFromAdmin'))->name('updateUserAdmin');
        
        // Change the user password
        Route::get('admin/change-user-password/{user_id}', 'Admin\UserController@changeUserPassword')->name('adminChangeUserPassword');
        Route::post('admin/change-user-password-submit', 'Admin\UserController@changeUserPasswordSubmit')->name('adminChangeUserPasswordSubmit'); 
        
        Route::get('users/patients/{id}/{pid}', 'Admin\UserController@patientDetail')->name('admin-patient');
        Route::get('users/patients/assessments/{id}/{pid}', 'Admin\UserController@assessments')->name('patient-assesments');
        Route::get('users/patients/agencies/{id}/{pid}', 'Admin\UserController@agencies')->name('patient-agencies');
        Route::get('users/patients/assessments/detail/{uid}/{pid}/{aid}', 'Admin\UserController@assessmentDetail')->name('patient-assesments-detail');
        Route::get('users/patients/agency/detail/{uid}/{pid}/{aid}', 'Admin\UserController@agencyDetail')->name('patient-agency-detail');
        Route::get('assessments/detail/{id}', 'Admin\AssessmentsController@detail')->name('assessment-detail');
        Route::get('assessments/list/{uid}', 'Admin\AssessmentsController@index')->name('assessment-detail-agency');
        // Route::get('hubs/detail/{id}', 'Admin\HubController@detail')->name('hub-details');
        Route::get('users/patients/hubs/{uid}/{pid}', 'Admin\UserController@hubLists')->name('hub-lists');
        Route::get('users/patients/hubs/details/{uid}/{pid}/{hid}', 'Admin\UserController@hubDetails')->name('hub-details');

        Route::get('services/agencies/{id}', 'Admin\ServiceController@agencies')->name('service-agency-list');
        Route::get('services/users/{id}', 'Admin\ServiceController@users')->name('service-user-list');
        Route::get('services/patients/{id}', 'Admin\ServiceController@patients')->name('service-patient-list');

        Route::get('admin/facilitiesrequest', 'Admin\FacilityRequestController@index') ->name('view-facilityrequest');
        // Home Page to open Dashboard
        //Dashboard data Ajax Call
        //Route::post('/user-graph', array('before' => 'csrf', 'uses' => 'Admin\AjaxController@userGraph'));
        Route::post('agencies/check', array('uses' => 'Admin\AjaxController@checkEmail'));

        ///  Route::post('settingUpdate', array( 'uses' => 'Admin\SettingController@store'));

        //Admin Profile Detail Pages
        Route::get('/admin-profile', array('before' => 'csrf', 'uses' => 'Admin\AdminProfileController@index'));

        // to Change data status in DB
        Route::post('/changeDataStatus', array('before' => 'csrf', 'uses' => 'Admin\AjaxController@changeDataStatus'));

        // to change payment_info_notReq in DB
        Route::post('/changePaymentReq', array('before' => 'csrf', 'uses' => 'Admin\AjaxController@changePaymentReq'));
        
        // Verion View
        Route::view('/versions', 'admin.appVersion.list');

        // Subscriptions View
        Route::view('/subscriptions', 'admin.subscription.list');

        // Notification View
        Route::view('/notification', 'admin.notification.list');

        // User Data table
        Route::get('/user-list', array('uses' => 'Admin\AjaxController@userData'));

        //patient data 
        Route::get('/patient-list', array('uses' => 'Admin\AjaxController@patientData'));

        // All Patient Data 
        Route::get('/patient-all', array('uses' => 'Admin\AjaxController@allPatient'));

        // All ShareCare Patient Data
        Route::get('/patient-sharecare', array('uses' => 'Admin\AjaxController@sharecarePatient'));

        // All ShareCare Patient Data
        Route::get('/patient-sharecare-matched', array('uses' => 'Admin\AjaxController@sharecarePatientsMatched'));

        //assessment data 
        Route::get('/assessment-list', array('uses' => 'Admin\AjaxController@assessmentData'));

        //all service agencies
        Route::get('/services-agency-list', array('uses' => 'Admin\AjaxController@serviceAgencyList'));

        //all service user
        Route::get('/services-user-list', array('uses' => 'Admin\AjaxController@serviceUserList'));

        //all service patient
        Route::get('/services-patient-list', array('uses' => 'Admin\AjaxController@servicePatientList'));

        //All assessment data 
        Route::get('/assessments-list', array('uses' => 'Admin\AjaxController@allAssessmentData'));
        //All hub Data
        Route::get('/hubs-list', array('uses' => 'Admin\AjaxController@allHubData'));
        // Patient hub data
        Route::get('/patient-hubs-list', array('uses' => 'Admin\AjaxController@patientHubData'))->name('patientHubsList');
        //All services data 
        Route::get('/services-list', array('uses' => 'Admin\AjaxController@servicesData'));

        //All services data 
        Route::get('/contacts-list', array('uses' => 'Admin\AjaxController@contactsData'));

        //agency data 
        Route::get('/agency-list', array('uses' => 'Admin\AjaxController@agencyData'));

        // Agency Data table
        Route::get('/agencies-list', array('uses' => 'Admin\AjaxController@agenciesData'));
        
        //Facility request data table
        Route::get('/facilitiesrequest-list', array('uses' => 'Admin\AjaxController@facilitiesrequestData'));
        
        //Facility request data
        Route::get('/facilityrequest-list', array('uses' => 'Admin\AjaxController@facilityrequestData'));


        // CMS Data table
        Route::get('/cms-list', array('uses' => 'Admin\AjaxController@cmsData'));
        //Payment gateway table

        Route::get('/payment-list', array('uses' => 'Admin\AjaxController@paymentData'));
        //Version List
        Route::get('/version-list', array('uses' => 'Admin\AjaxController@versionList'));

        //notification
        Route::get('/notification-list', array('uses' => 'Admin\AjaxController@notificationList'));

        //sub Admin
        Route::get('/users-list', array('uses' => 'Admin\AjaxController@subAdminData'));

        //Agency roles
        //sub Admin
        Route::get('/agency-data', array('uses' => 'Admin\AjaxController@agencyRole'));


        //sub subscription
        Route::get('/subscription-list', array('uses' => 'Admin\AjaxController@subscriptionData'));


        //sub subscription
        Route::post('/add-subscription', array('uses' => 'Admin\SubscriptionsController@AddSubscriptions'));

        //sub subscription
        Route::post('/update-subscription', array('uses' => 'Admin\SubscriptionsController@UpdateSubscriptions'));

        //
        Route::get('/export-user-data', ['uses' => 'Admin\UserController@exportUserData']);

        // Send Notification
        Route::get('send-notification/{id}', ['uses' => 'Admin\NotificationController@requestNotification']);

        // Change Password
        Route::view('change-password', 'admin.adminProfile.changePassword');

        // Send Notification
        Route::post('change-password', ['before' => 'csrf', 'uses' => 'Admin\AdminProfileController@changePassword']);


        // Send user Notification
        Route::post('send-user-notification', ['before' => 'csrf', 'uses' => 'Admin\AjaxController@sendUserNotification']);

        // Dash Board
        Route::get('/', array('uses' => 'Admin\AdminController@index'));

        // Cropper
        Route::view('cropper', 'admin.cropper.cropper');

        //  Route::post('transfer-image', ['before' => 'csrf', 'uses' => 'Admin\CroppedImageUploader@uploadImage']);

        Route::post('/', array('before' => 'csrf', 'uses' => 'Admin\AdminController@login'));

        // Send Notification
        Route::post('check-role', ['before' => 'csrf', 'uses' => 'Admin\AjaxController@checkRole']);

        // ShareCare - add patient from pending list to hub (admin)
        Route::post('action/admin-member-request', 'Common\HubController@actionOnMemberRequest')->name('admin_hub_action_member_request');
        // ShareCare - invite nearby members to hub (admin)
        Route::post('sent/admin-request', 'Common\HubController@sendrequestToUser')->name('admin-sent-request');
        // ShareCare - request service from nearby agency (admin)
        Route::post('/admin-request-agency', 'Common\HubController@actionOnAgencyRequest')->name('admin_request_agency');
        // ShareCare - edit hub details from hub details page (admin)
        Route::post('/admin-edit', 'Admin\HubsController@editHubInfo')->name('adminEditHubInfo');
    }
);

/**
 * Provider Routes
 */

//public
Route::get('/doctor-visit', 'Provider\PublicController@index')->name('doctor.visit');
Route::post('/doctor-visit', 'Provider\PublicController@send');


//patient
Route::group(['middleware' => ['user', 'checkBlocked']], function () {
    Route::get('dashboard/doctor-visits/{pid}', 'Provider\PatientController@index')->name('indexDoctorVisits');
    Route::get('dashboard/doctor-visits/create/{pid}', 'Provider\PatientController@create')->name('requestDoctorVisits');
    Route::get('dashboard/doctor-visits/{pid}/{appointment_id}', 'Provider\PatientController@show')->name('showDoctorVisits');
    Route::post('dashboard/doctor-visits/create/{pid}', 'Provider\PatientController@store')->name('storeDoctorVisits');
    Route::delete('dashboard/doctor-visits', 'Provider\PatientController@destroy')->name('cancelProviderAppointment');
});

//Doctor
Route::group(['middleware' => ['auth', 'checkBlocked', 'provider']], function () {
    //appointments
    Route::get('hc-provider/dashboard', 'Provider\ProviderAppointmentController@index')->name('hcProviderDashboard');
    Route::get('hc-provider/dashboard/appointment/{id}', 'Provider\ProviderAppointmentController@show')->name('hcProviderDashboardAppointmentShow');
    Route::get('hc-provider/dashboard/appointment/complete/{id}', 'Provider\ProviderAppointmentController@markComplete')->name('hcProviderDashboardAppointmentComplete');
    Route::delete('hc-provider/dashboard/doctor-visits', 'Provider\PatientController@destroy')->name('providerCancelProviderAppointment');
    Route::post('hc-provider/appointment/notes/{id}', 'Provider\AppointmentNoteController@store')->name('hcProviderCreateAppointmentNote');
    Route::post('hc-provider/patient/notes/{id}', 'Provider\PatientNoteController@store')->name('hcProviderCreatePaitentNote');
    Route::put('hc-provider/appointment/schedule/{id}', 'Provider\ProviderAppointmentController@schedule')->name('hcProviderScheduleAppointment');
    //patients
    Route::get('hc-provider/dashboard/patients', 'Provider\ProviderPatientController@index')->name('hcProviderDashboardPatients');
    Route::get('hc-provider/dashboard/patients/{id}', 'Provider\ProviderPatientController@show')->name('hcProviderDashboardPatientsShow');
    Route::get('hc-provider/dashboard/patients/{id}/notes', 'Provider\ProviderPatientController@showNotes')->name('hcProviderDashboardPatientsShowNotes');
    Route::get('hc-provider/dashboard/patients/{id}/appointments', 'Provider\ProviderPatientController@showAppointments')->name('hcProviderDashboardPatientsShowAppointments');
    Route::post('hc-provider/patient/notes-no-appt/{id}', 'Provider\PatientNoteController@storeNoAppointment')->name('hcProviderCreatePaitentNoteNoAppt');
    Route::post('hc-provider/patient', 'Provider\ProviderPatientController@storePatient')->name('hcProviderCreatePaitent');
    Route::put('hc-provider/patient/{id}', 'Provider\ProviderPatientController@updatePatient')->name('hcProviderUpdatePaitent');
    Route::get('hc-provider/patient/create', 'Provider\ProviderPatientController@createPatient')->name('hcProvideCreatePaitentForm');
    Route::post('hc-provider/patient/appointment/{id}', 'Provider\ProviderPatientController@storeAppointment')->name('hcProviderNewAppointment');
});

// Ajax Call for Forgot Password
Route::post('/forgot_password', array('before' => 'csrf', 'uses' => 'Admin\AjaxController@forgotPassword'));

Route::post('/checkEmail', array('before' => 'csrf', 'uses' => 'Admin\AjaxController@checkUserEmail'));
Route::post('/checkPhone', array('before' => 'csrf', 'uses' => 'Admin\AjaxController@checkUserPhone'));
Route::post('/checkEmailAgency', array('before' => 'csrf', 'uses' => 'Admin\AjaxController@checkEmailAgency'));
Route::post('/checkEmailPartner', array('before' => 'csrf', 'uses' => 'Admin\AjaxController@checkEmailPartner'));

Route::post('/set-cookie', array('before' => 'csrf', 'uses' => 'Admin\AjaxController@setCookie'));

Auth::routes();
Route::redirect('/login', '/');  //used to redirect to base url 

// Payment after billing
Route::get('add-card-invoice/{id?}', array('uses' => 'User\HomeController@invoice_newcard'))->name('invoicenewcard');
Route::post('invoice-submit', 'User\HomeController@billInfoSubmit')->name('invoiceSubmit');

//Default website
Route::get('/', array('uses' => 'User\HomeController@index'));

Route::get('homecare/old', 'User\HomeController@index')->name('old-homecare');


Route::get('/agency/details/{aid}/{cid?}', 'User\HomeController@getAgencyDetails')->name('agencydetails');


Route::get('/personal/care-submit', 'User\HomeController@personalsubmit')->name('SubmitPerosnalCare');
Route::post('/personal/care-submit', 'Admin\AdminController@personalsubmit')->name('SubmitPersonalCare');
Route::post('/send-noagency-found-mail', 'User\HomeController@sendNoAgencyFoundMail')->name('sendNoAgencyFoundMail');

Route::prefix('admin')->group(function () {
    // Login Page
    Route::get('/', array('as' => 'loginadmin', 'uses' => 'Admin\AdminController@index'));
});

// Admin user create
Route::post('/users/create', 'Admin\AdminController@createUser')->name('createUser');

// Admin edit patient
// Route::post('users/updateUserAdmin', array('uses' => 'User\UserAccountsettingController@updateUserFromAdmin'))->name('updateUserAdmin');

// Admin create card
// Route::post('admin-add-card', array('uses' => 'User\UserAccountsettingController@adminnewcard'))->name('adminnewcard');

// Promotion Routes
Route::get('/gift', 'User\PromoController@index')->name('viewPromotion');
// NEW PROMO ROUTES
Route::get('/splash/{care_id}/{city?}', 'User\PromoController@index')->name('promoView');
Route::post('/new_promo_submit', 'User\PromoController@store')->name('promoSubmit');
Route::get('/promo_search_results', 'User\PromoController@search')->name('promoSearch');

// Old Promotion Routes
Route::get('/gift', 'User\PromoController@index');
// ->name('viewPromotion');
Route::post('/gift', 'User\PromoController@store')->name('promotionSubmit');

Route::get('/mississauga-promo', 'User\PromoController@index');
// ->name('viewPromotion');
// Route::post('/mississauga-promo', 'User\PromoController@store')->name('promotionSubmit');

// PARTNER
Route::group(['prefix' => 'partner', 'middleware' => 'partner'], function () {
    //Agency signin
    Route::get('signin/{id?}', array('uses' => 'Partner\SigninController@index'))->name('partnersignin');
    // Signin Page
    Route::post('signin', array('as' => 'login', 'uses' => 'Partner\SigninController@login'));

    //Agency sign up
    Route::get('signup', array('uses' => 'Partner\SignupController@index'))->name('partnersignup');
    //Signup user submit    
    Route::post('signup', array('as' => 'signup', 'uses' => 'Partner\SignupController@create'));

    // Partner User Profile
    Route::get('user/profile', 'Partner\PartnerController@partnerUserProfile')->name('partneruserprofile');
    Route::post('user/profile/submit', 'Partner\PartnerController@partnerUserProfileSubmit')->name('partneruserprofilesubmit');

    // Go to Partner Profile
    Route::get('profile', 'Partner\PartnerController@partnerProfile')->name('partnerprofile');
    Route::post('profile/submit', 'Partner\PartnerController@partnerProfileSubmit')->name('partnerprofilesubmit');

    // Dashboard
    Route::get('home/{name?}', 'Partner\AssessmentController@index')->name('partnerhome');
    Route::get('home/blocked', 'Partner\AssessmentController@blocked')->name('partnerblocked');
    Route::get('home/inactive', 'Partner\AssessmentController@deleted')->name('partnerdeleted');
    Route::get('agencies', 'Partner\AssessmentController@showAgencies')->name('agencyhome');

    Route::get('welcome', function () {
        return view('agency.registration.welcome');
    });

    //partner-patients
    Route::get('patients/all/{type}/{name?}', 'Partner\AssessmentController@showPatientType')->name('partner_all_patient')->where(['type' => '[aphi]+']);
    Route::get('patients/active/{type}/{name?}', 'Agency\AssessmentController@showPatientType')->name('partner_active_patient')->where(['type' => '[aphi]+']);
    Route::get('patients/historical/{type}/{name?}', 'Agency\AssessmentController@showPatientType')->name('partner_historical_patient')->where(['type' => '[aphi]+']);
    Route::get('patients/inprogress/{type}/{name?}', 'Agency\AssessmentController@showPatientType')->name('partner_progress_patient')->where(['type' => '[aphi]+']);
    Route::get('patients/pending/{type}/{name?}', 'Agency\AssessmentController@showPatientType')->name('partner_pending_patient')->where(['type' => '[aphi]+']);
    
    //Route::get('invoices/{type}/{aid?}', 'Agency\InvoiceController@getInvoices')->name('show_invoice');
    Route::get('invoices/patients/{bid}/{date}', 'Agency\InvoiceController@getPatientInvoices')->name('patient_sharecare_bill');
    Route::get('generate/pdf', 'Agency\InvoiceController@generateInvoice')->name('htmltopdfview');

    //Assessments steps
    Route::get('patient/{pid}/{name?}', 'Partner\AssessmentController@patientassessments')->name('partner_pa');
    Route::get('step1-6/{patientid}/{cid?}/{uid?}', 'Partner\PartnerController@step1view')->name('partnerBeginStep1');
    Route::post('/step1-submit', 'Partner\AssessmentController@step1viewsubmit')->name('partnersubmitstep1');
    Route::get('assessment/pending/list/{name?}', 'Partner\AssessmentController@allpandings')->name('partner_ap');
    Route::get('assessment/inprogress/list/{name?}', 'Partner\AssessmentController@allinprogress')->name('partner_aip');
    Route::get('assessment/complete/list/{name?}', 'Partner\AssessmentController@allcomplete')->name('partner_ac');
    Route::get('assessment/active/list/{name?}', 'Partner\AssessmentController@allactive')->name('partner_aa');
    Route::get('assessment/approved/list/{name?}', 'Partner\AssessmentController@allapproved')->name('partner_approved');
    Route::post('assessment/add-notes', 'Partner\AssessmentController@addNotes')->name('addnotespartner');

    // Users Tab
    Route::get('users/view/{id}/{pid?}/{routeName?}', 'Partner\AssessmentController@viewuser')->name('partnerUserView');
    Route::get('users/patients/{name?}', 'Partner\AssessmentController@viewpatients')->name('partnerPatientsView');
    Route::get('users/patients/findcare/{id}/{pid}', 'Partner\PartnerController@findcare')->name('partnerFindCare');
    Route::get('/personal/care-submit', 'Partner\PartnerController@personalsubmit')->name('partnerSubmitCare');
    Route::get('/agency/details/{aid}/{cid?}/{uid?}/{pid?}', 'Partner\PartnerController@getAgencyDetails')->name('partnerAgencyDetail');
    Route::post('partner-request-assessment', 'Partner\PartnerController@selectpatientview')->name('partnerRequestAssessment');
    Route::get('assessment/view/{id}/{routeName?}', 'Partner\AssessmentController@viewassessment')->name('partnerav');
    Route::post('add-patient-step1/submit', 'Partner\AssessmentController@addpatientsubmit')->name('partneraddpviewsubmit');

    // Partner user create
    Route::post('/users/create', 'Partner\PartnerController@createUser')->name('partnerCreateUser');
    
    // Partner create
    Route::post('add-patient/submit', 'Partner\PartnerController@createPatient')->name('partnerCreatePatient');
    Route::post('edit-patient/submit', 'Partner\PartnerController@editPatient')->name('partnerEditPatient');

    // Partner, change user's password
    Route::get('user/change/password/{user_id}', 'Partner\AssessmentController@changeUserPassword')->name('partnerUserChangePassword');
    Route::post('user/change/password/submit', 'Partner\AssessmentController@changeUserPasswordSubmit')->name('partnerUserPasswordSubmit');

    // Search Filter
    Route::post('patients/search-user', 'Partner\AssessmentController@searchUser')->name('parter_su');
    Route::post('patients/search-patient', 'Partner\AssessmentController@searchPatient')->name('parter_sp');
    Route::post('patients/search-assessment', 'Partner\AssessmentController@searchAssessment')->name('parter_sa');
    // Partner Landing
    // Route::get('assessment/pending/list/{name?}', 'Partner\AssessmentController@allpandings')->name('partner_ap');
    
    // User Profile
    Route::get('user-account-setting/{id?}/{pid?}', array('uses' => 'Partner\PartnerController@userProfile'))->name('partner_user_acc');
    Route::post('user-account-submit/{id?}', array('uses' => 'Partner\PartnerController@updateUser'))->name('partner_user_submit');
    Route::get('user-billing-info/{is_complete?}/{id?}', array('uses' => 'Partner\PartnerController@billingInfo'))->name('partnerbillinginfo');
    Route::get('add-new-card/{uid}', array('uses' => 'User\UserAccountsettingController@partner_add_card'))->name('partner_add_card');
    Route::post('set-default-card/{uid}', array('uses' => 'Partner\PartnerController@set_default_card'))->name('partnerDefaultCard');
    Route::post('user-card-delete/{uid}', array('uses' => 'Partner\PartnerController@deleteCard'))->name('partnerdeletecard');
    Route::post('user-billing-info-submit/{id?}', array('uses' => 'Partner\PartnerController@saveBill'))->name('partnerbillingupdate');
    Route::post('user-card-info-submit/{id?}', array('uses' => 'Partner\PartnerController@savenewcard'))->name('partner_card');
    Route::get('edit-user-billing-info/{id?}', array('uses' => 'Partner\PartnerController@editbillingInfo'))->name('partnerbillinginfoedit');
    Route::post('edit-user-billing-info-submit/{id?}', array('uses' => 'Partner\PartnerController@updateBill'))->name('partnerbillinginfoupdate');
    Route::get('user-account-history/{id?}', array('uses' => 'Partner\PartnerController@billingHistory'))->name('partnerbillinghistory');
});

Route::group(['prefix' => 'agency', 'middleware' => 'agency'], function () {
    //Agency signin
    Route::get('signin/{id?}', array('uses' => 'Agency\SigninController@index'))->name('agencysignin');
    // Signin Page
    Route::post('signin', array('as' => 'login', 'uses' => 'Agency\SigninController@login'));

    //Agency sign up
    Route::get('signup', array('uses' => 'Agency\SignupController@index'))->name('agencysignup');
    //Signup user submit    
    Route::post('signup', array('as' => 'signup', 'uses' => 'Agency\SignupController@create'));

    Route::get('welcome', function () {
        return view('agency.registration.welcome');
    });
});

Route::group(['prefix' => 'agency', 'middleware' => ['auth', 'agency', 'checkBlocked']], function () {

    /**
     * AI577 route define
     */
    // declare route to access agency profile
    Route::get('user/profile', 'Agency\ProfileController@agencyUserProfile')->name('agencyuserprofile');

    Route::post('user/profile/submit', 'Agency\ProfileController@agencyUserProfileSubmit')->name('agencyuserprofilesubmit');

    Route::get('profile', 'Agency\ProfileController@agencyProfile')->name('agencyprofile');

    Route::post('profile/submit', 'Agency\ProfileController@agencyProfileSubmit')->name('agencyprofilesubmit');

    Route::get('billing/info', 'Agency\ProfileController@agencyBillingInfo')->name('agencybillingindex');
    Route::get('sharecare', 'Agency\ProfileController@agencyShareCare')->name('agencysharecare');

    Route::get('add-sub-user', 'Agency\ProfileController@subUserAddView')->name('createsubuserview');

    Route::post('add-sub-user-submit', 'Agency\ProfileController@subUserAddViewSubmit')->name('agencysubuserprofilesubmit');
    Route::post('add-sub-user-update', 'Agency\ProfileController@subUserUpdateViewSubmit')->name('agencysubuserprofileupdate');

    Route::get('agentlist', 'Agency\ProfileController@allAgentList')->name('showagentlist');

    Route::post('add-sub-user-update-submit', 'Agency\ProfileController@subUserAddViewUpdateSubmit')->name('agencysubuserprofileupdatesubmit');

    //Assessments steps
    Route::get('assessment/pending/list/{name?}', 'Agency\AssessmentController@allpandings')->name('assessmentpandingslist');
    Route::get('assessment/inprogress/list/{name?}', 'Agency\AssessmentController@allinprogress')->name('assessmentinprogress');
    Route::get('assessment/denied/list/{name?}', 'Agency\AssessmentController@alldenied')->name('assessmentdeniedrequests');
    Route::get('assessment/active/list/{name?}', 'Agency\AssessmentController@allactive')->name('assessmentactive');


    Route::get('assessment/view/{id}/{routeName?}', 'Agency\AssessmentController@viewassessment')->name('assessmentview');
    Route::post('assessment/change-hours-submit/{ref?}', 'Agency\AssessmentController@change_hours')->name('change_hours');

    Route::get('assessment/accept/{id}', 'Agency\AssessmentController@acceptassessment')->name('acceptaccessment');

    Route::post('assessment/active', 'Agency\AssessmentController@activcassessment')->name('active_assessment_submit');
    Route::post('assessment/update', 'Agency\AssessmentController@updateassessment')->name('update_assessment_submit');


    Route::post('schedule-assessment-submit', 'Agency\AssessmentController@scheduleAssessmentSubmit')->name('schedule_assessment_submit');
    Route::post('bill-info-submit', 'Agency\AssessmentController@billInfoSubmit')->name('bill_info_submit');

    Route::post('assessment/complete', 'Agency\AssessmentController@completeassessment')->name('complete_assessment_submit');

    Route::post('save-assessment-submit', 'Agency\AssessmentController@saveAssessmentSubmit')->name('save_assessment_submit');

    Route::get('assessment/decline/{id}', 'Agency\AssessmentController@declineassessment')->name('declineaccessment');
    Route::get('assessment/send-alert/{id}/{type}', 'Agency\AssessmentController@sendalert')->name('sendAlert');

    Route::post('assessment/declineassessment', 'Agency\AssessmentController@declineassessmentPost')->name('decline_assessment');

    Route::post('assessment/add-notes', 'Agency\AssessmentController@addNotes')->name('addnotesurl');
    Route::post('assessment/get-bill', 'Agency\AssessmentController@getBills')->name('getbillsurl');
    Route::post('assessment/check-bill', 'Agency\AssessmentController@checkBills')->name('checkbillsurl');
    Route::post('assessment/search-assessment', 'Agency\AssessmentController@searchAssessment')->name('search_assessment');
    
    
    Route::get('change/password', 'Agency\ProfileController@changepasswordview')->name('agency_change_password');
    
    Route::post('change/password/submit', 'Agency\ProfileController@changePasswordViewSubmit')->name('agency_passwordsubmit');
    
    // Create User/Patient and Assessment
    Route::get('new-user', 'Agency\AssessmentController@createUserScheduleAssessment')->name('createUserScheduleAssessment');
    Route::post('new-user/create', 'Agency\AssessmentController@createUserScheduleAssessmentSubmit')->name('createUserScheduleAssessmentSubmit');

    
    //agency-patients
    Route::get('patients/all/{type}/{name?}', 'Agency\AssessmentController@showPatientType')->name('show_all_patient')->where(['type' => '[aphi]+']);
    Route::get('patients/active/{type}/{name?}', 'Agency\AssessmentController@showPatientType')->name('show_active_patient')->where(['type' => '[aphi]+']);
    Route::get('patients/historical/{type}/{name?}', 'Agency\AssessmentController@showPatientType')->name('show_historical_patient')->where(['type' => '[aphi]+']);
    Route::get('patients/inprogress/{type}/{name?}', 'Agency\AssessmentController@showPatientType')->name('show_progress_patient')->where(['type' => '[aphi]+']);
    Route::get('patients/pending/{type}/{name?}', 'Agency\AssessmentController@showPatientType')->name('show_pending_patient')->where(['type' => '[aphi]+']);
    Route::post('patients/search-patient', 'Agency\AssessmentController@searchPatient')->name('search_patient');
    
    Route::get('invoices/{type}/{aid?}', 'Agency\InvoiceController@getInvoices')->name('show_invoice');
    Route::get('invoices/patients/{bid}/{date}', 'Agency\InvoiceController@getPatientInvoices')->name('patient_sharecare_bill');
    Route::get('generate/pdf', 'Agency\InvoiceController@generateInvoice')->name('htmltopdfview');

    Route::get('search-invoice', 'Agency\InvoiceController@serachInvoice')->name('search_invoice');
    Route::get('update/agent/{id}', 'Agency\ProfileController@updateAgent')->name('deactiveagent');
    Route::get('messages/{id?}/{pid?}', array('uses' => 'Agency\MessageController@index'))->name('messagesagency');
    Route::post('messages/add-file-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@add'])->name('addFile');
    Route::post('/messages/check-file-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@check']);
    Route::post('/messages/update-read-status-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@updateReadStatus']);
    Route::post('/messages/transfer-file-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@uploads']);
    Route::post('/messages/search-user', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@searchUser']);
    Route::post('search-user', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@searchUser']);
    Route::post('search-contact', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@searchContact']);


    Route::post('/messages/check-last-data-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@checkLastData']);
    Route::post('check-last-data-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@checkLastData']);
    Route::post('add-file-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@add']);
    Route::post('check-file-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@check']);
    Route::post('transfer-file-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@uploads']);
    Route::post('recievers-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@recievers']);
    Route::post('/messages/recievers-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@recievers']);
    Route::post('read-msg-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@readMsg']);
    Route::post('/messages/read-msg-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@readMsg']);
    Route::post('chk-notemsg-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@chkNoteMsg']);
    Route::post('/messages/chk-notemsg-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@chkNoteMsg']);
    Route::post('check-email', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@checkEmail']);
    Route::post('/messages/check-last-data-patient-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@checkLastDataPatient']);
    Route::post('/messages/set-timezone', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@setTimeZone']);

    Route::get('notifications', ['uses' => 'Agency\NotificationController@index'])->name('notificationsagency');
    Route::get('notifications/all/{search?}', ['uses' => 'Agency\NotificationController@allnotificationlist'])->name('allnotificationlistagency');
    Route::get('notifications/viewed/{search?}', ['uses' => 'Agency\NotificationController@readnotificationlist'])->name('readnotificationlistagency');
    Route::get('notifications/recent/{search?}', ['uses' => 'Agency\NotificationController@unreadnotificationlist'])->name('unreadnotificationlistagency');
    Route::post('assessment/search-notification-agency', 'Agency\NotificationController@searchNotification')->name('search_notification_agency');

    Route::post('share-careread-note-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@readNote'])->name('readNoteAgency');
    Route::post('notifications/read-note-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@readNote'])->name('readNoteAgency');
    Route::post('notifications/read-note-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@readNote'])->name('readNoteAgency');
    Route::post('/messages/side-list-agency', ['before' => 'csrf', 'uses' => 'Agency\AjaxController@sideList']);

    Route::get('share-care', 'Agency\SharecareController@index')->name('sharecare');
    Route::get('cancelCare/{hid?}', 'Agency\SharecareController@cancelCare')->name('cancel-care');
    Route::get('stopCare/{hid?}', 'Agency\SharecareController@stopCare')->name('stop-care');

    Route::get('share-care/active', 'Agency\SharecareController@activeHub')->name('sharecareActive');
    Route::get('share-care/denied', 'Agency\SharecareController@deniedHub')->name('sharecareDenied');
    Route::get('share-care-detail/{hid?}', 'Agency\SharecareController@detail')->name('sharecaredetail');
    Route::post('sharecare-bill-info-submit', 'Agency\SharecareController@billInfoSubmit')->name('sharecare_bill_info_submit');
    Route::post('sharecare-get-bill', 'Agency\SharecareController@getBills')->name('sharecarebillurl');
    Route::post('sharecare-check-bill', 'Agency\SharecareController@checkBills')->name('sharecarecheckbillsurl');

    Route::post('price/submit', 'Agency\ProfileController@priceSubmit')->name('price-submit');
    Route::post('save-agency-request', 'Agency\SharecareController@saveAgencyRequest')->name('saveAgencyRequest');
    Route::get('change-approved-agency/{status?}/{mid?}', 'Agency\SharecareController@changeApprovedByAgency')->name('changeApprovedByAgency');
});

// Move to Admin Group
Route::post('/decline', 'User\AssessmentController@declineassessment')->name('admindeclineaccessment');
Route::post('/admin-step1-submit', 'User\AssessmentController@step1viewsubmit')->name('adminsubmitstep1');
Route::post('admin-request-assessment', 'User\AssessmentController@adminselectpatientview')->name('adminRequestAssessment');
Route::get('/adminagency/details/{aid}/{cid?}/{uid?}/{pid?}', 'User\HomeController@getAdminAgencyDetails')->name('adminagencydetails');
Route::get('users/patients/assessments/findcare/{id}/{pid}', 'Admin\AdminController@findcare')->name('findcare');
Route::post('admin-add-patient-step1/submit', 'User\PatientDashboardController@adminaddpatientsubmit')->name('adminaddpviewsubmit');
Route::get('admin-add-patient-step1/{pid?}/{key?}/{parameters?}', 'User\PatientDashboardController@adminaddpatientview')->name('adminaddpatientinfo');
Route::get('admin-edit-patient-step1/{pid?}/{userid?}', 'User\PatientDashboardController@adminaddpatientview')->name('admineditpatientinfo');
// Route::post('admin-new-card', array('uses' => 'User\UserAccountsettingController@savenewcard'))->name('adminsavenewcard');
// Route::post('admin-user-card-delete', array('uses' => 'User\UserAccountsettingController@deleteCard'))->name('adminuserdeletecard');

Route::group(['prefix' => 'user', 'middleware' => ['user', 'checkBlocked']], function () {
    Route::post('check-year', ['before' => 'csrf', 'uses' => 'User\AjaxController@checkYear']);
    Route::post('search-user', ['before' => 'csrf', 'uses' => 'User\AjaxController@searchUser']);
    Route::post('search-user-list', ['before' => 'csrf', 'uses' => 'User\AjaxController@searchUserlist']);
    Route::post('search-patient-contact', ['before' => 'csrf', 'uses' => 'User\AjaxController@searchPatientContact']);

    // User signin
    Route::get('generate/pdf', 'User\UserAccountsettingController@generateInvoice')->name('htmltopdfviewUser');

    Route::get('/signin/{id?}/{route?}/{promo?}', 'User\SigninController@index')->name('usersignin');
    //Route::get('/', array('uses' => 'User\SigninController@index'));
    
    // Signin Page
    Route::post('signin/', array('as' => 'login', 'uses' => 'User\SigninController@login'));
    
    // User sign up
    Route::get('signup/{route?}', array('uses' => 'User\SignupController@index'))->name('usersignup');
    
    // User sign up using MOBILE NUMBER
    Route::get('mobilesignup/{route?}', array('uses' => 'User\SignupController@mobileIndex'))->name('phoneUserSignUp');
    
    // Signup user submit    
    Route::post('signup', array('as' => 'signup', 'uses' => 'User\SignupController@create'));
    
    // Signup user submit    
    Route::post('mobilesignup', array('as' => 'signup', 'uses' => 'User\SignupController@mobileCreate'));
    
    // After signup redirect on welcome page
    Route::get('welcome', function () {
        return view('user.registration.welcome');
    })->name('userWelcome');
    Route::get('welcome-ltc', function () {
        return view('user.registration.welcomeLTC');
    });

    Route::get('success', 'User\UserprofileController@checkCompleteProfile')->name('getsuccess');
    //User profilesetup
    Route::get('user-profile', array('uses' => 'User\UserprofileController@index'))->name('userprofile');
    //Signup user profile    
    Route::post('user-profile', array('as' => 'user-profile', 'uses' => 'User\UserprofileController@create'));

    //Patient profilesetup

    Route::get('user-account-setting', array('uses' => 'User\UserAccountsettingController@index'))->name('useraccountsetting');
    Route::post('user-account-setting', array('uses' => 'User\UserAccountsettingController@updateUser'))->name('useraccountsettingupdate');
    Route::get('user-billing-info/{is_complete?}', array('uses' => 'User\UserAccountsettingController@billingInfo'))->name('userbillinginfo');
    Route::post('user-billing-info-submit', array('uses' => 'User\UserAccountsettingController@saveBill'))->name('userbillinginfoupdate');

    Route::post('user-card-delete', array('uses' => 'User\UserAccountsettingController@deleteCard'))->name('userdeletecard');

    Route::get('edit-user-billing-info', array('uses' => 'User\UserAccountsettingController@editbillingInfo'))->name('userbillinginfoedit');
    Route::post('edit-user-billing-info-submit', array('uses' => 'User\UserAccountsettingController@updateBill'))->name('userbillinginfoupdate');
    Route::get('add-new-card', array('uses' => 'User\UserAccountsettingController@add_newcard'))->name('addnewcard');
    Route::post('add-new-card-submit', array('uses' => 'User\UserAccountsettingController@savenewcard'))->name('savenewcard');
    Route::post('set-default-card', array('uses' => 'User\UserAccountsettingController@set_default_card'))->name('setDefaultCard');

    Route::get('user-account-history/{bid?}', array('uses' => 'User\UserAccountsettingController@billingHistory'))->name('userbillinghistory');
    Route::get('user-account-history-sharecare/{hubid?}', array('uses' => 'User\UserAccountsettingController@billingHistorySharecare'))->name('billinghistorysharecare');

    Route::get('invoices/{type}/{hubid}', 'User\UserAccountsettingController@getInvoices')->name('show_invoice_user');

    Route::get('patient-profile/{aid?}/{type?}', array('uses' => 'User\UserprofileController@patientprofile'))->name('mailpatientprofile');
    //Patient user profile    
    Route::post('patient-profile', array('as' => 'patient-profile', 'uses' => 'User\UserprofileController@store'));

    //Patient profilesetup
    Route::get('additional-info/{aid?}/{cid?}', array('uses' => 'User\UserprofileController@additionalinfo'));
    //Patient user profile    
    Route::post('additional-info', array('as' => 'additional-info', 'uses' => 'User\UserprofileController@additional'));

    Route::get('patient/dashboard/{pid?}', 'User\PatientDashboardController@index')->name('patientdashboard');
    Route::get('addpatientinfo/{pid?}/{cid?}', 'User\PatientDashboardController@addpatientinfoview')->name('add_patient_info');

    Route::post('addpatientinfo', 'User\PatientDashboardController@addpatientinfo')->name('add_patient_info');

    Route::get('dashboard/saved-agency/{pid}', 'User\PatientDashboardController@showSavedAgencyView')->name('showSavedAgencies');

    // Route::get('dashboard/feedback-history/{pid}', 'User\PatientDashboardController@showSavedAgencyView')->name('showSavedAgencies');

    Route::get('dashboard/historical-agency/{pid}', 'User\PatientDashboardController@showHistoricalAgencyView')->name('showHistoricalAgency');

    Route::get('dashboard/share-care-hub/{pid}', 'User\PatientDashboardController@showShareCareHubView')->name('show_Share_Care_Hub_View');

    Route::get('userdashboard', 'User\UserDashboardController@index')->name('userdashboard');
    Route::get('userdashboard/saved-agency/', 'User\UserDashboardController@showSavedAgencyView')->name('showUserSavedAgencies');
    Route::get('userdashboard/share-care-hub/', 'User\UserDashboardController@showShareCareHubView')->name('showUserSavedHubs');

    Route::get('add-patient-step1/{pid?}/{key?}/{parameters?}', 'User\PatientDashboardController@addpatientview')->name('addpview');
    Route::post('add-patient-step1/submit', 'User\PatientDashboardController@addpatientsubmit')->name('addpviewsubmit');

    Route::get('add-patient-step2/{id}/{key?}/{parameters?}', 'User\PatientDashboardController@addpatientstep2view')->name('addpviewstep2');
    Route::post('add-patient-step2/submit', 'User\PatientDashboardController@addpatientstep2viewsubmit')->name('addpviewstep2submit');

    Route::get('add-patient-step3/{id}/{key?}/{parameters?}', 'User\PatientDashboardController@addpatientstep3view')->name('addpviewstep3');
    Route::post('add-patient-step3/submit', 'User\PatientDashboardController@addpatientstep3viewsubmit')->name('addpviewstep3submit');

    Route::get('edit-patient/{id}', 'User\PatientDashboardController@addpatientstep2view')->name('edit-patient-profile');

    /* Feedback Module */
    Route::get('feedback/{id?}/{hid?}', array('uses' => 'User\FeedbackController@index'))->name('feedbackhub');

    /* Message Module */
    Route::get('messages/{id?}/{hid?}/{pid?}', array('uses' => 'User\MessageController@index'))->name('messagesuser');
    Route::get('messages/{id?}/{hid?}', array('uses' => 'User\MessageController@index'))->name('messageshub');
    Route::post('/messages/add-file', ['before' => 'csrf', 'uses' => 'User\AjaxController@add']);
    Route::post('/messages/check-file', ['before' => 'csrf', 'uses' => 'User\AjaxController@check']);
    Route::post('/messages/update-read-status', ['before' => 'csrf', 'uses' => 'User\AjaxController@updateReadStatus']);

    Route::post('/messages/transfer-file', ['before' => 'csrf', 'uses' => 'User\AjaxController@uploads']);
    // Route::post('/user/messages/add-file', ['before' => 'csrf', 'uses' => 'User\AjaxController@add']);
    Route::post('check-file', ['before' => 'csrf', 'uses' => 'User\AjaxController@check']);
    Route::post('transfer-file', ['before' => 'csrf', 'uses' => 'User\AjaxController@uploads']);
    Route::post('recievers', ['before' => 'csrf', 'uses' => 'User\AjaxController@recievers']);
    Route::post('/messages/recievers', ['before' => 'csrf', 'uses' => 'User\AjaxController@recievers']);
    Route::post('/messages/check-last-data', ['before' => 'csrf', 'uses' => 'User\AjaxController@checkLastData']);
    Route::post('check-last-data', ['before' => 'csrf', 'uses' => 'User\AjaxController@checkLastData']);
    Route::post('/messages/check-last-data-patient', ['before' => 'csrf', 'uses' => 'User\AjaxController@checkLastDataPatient']);
    Route::post('/messages/set-timezone', ['before' => 'csrf', 'uses' => 'User\AjaxController@setTimeZone']);
    Route::post('/messages/show-more', ['before' => 'csrf', 'uses' => 'User\AjaxController@showMoreData']);

    Route::post('chk-notemsg', ['before' => 'csrf', 'uses' => 'User\AjaxController@ChkNoteMsg']);
    Route::post('/messages/chk-notemsg', ['before' => 'csrf', 'uses' => 'User\AjaxController@ChkNoteMsg']);
    Route::post('read-msg', ['before' => 'csrf', 'uses' => 'User\AjaxController@readMsg']);
    Route::post('/messages/read-msg', ['before' => 'csrf', 'uses' => 'User\AjaxController@readMsg']);
    Route::post('/messages/side-list-user', ['before' => 'csrf', 'uses' => 'User\AjaxController@sideList']);

    Route::get('notifications', ['uses' => 'User\NotificationController@index'])->name('notificationsuser');
    Route::get('notifications/all/{search?}', ['uses' => 'User\NotificationController@allnotificationlist'])->name('allnotificationlistuser');
    Route::get('notifications/viewed/{search?}', ['uses' => 'User\NotificationController@readnotificationlist'])->name('readnotificationlistuser');
    Route::get('notifications/recent/{search?}', ['uses' => 'User\NotificationController@unreadnotificationlist'])->name('unreadnotificationlistuser');
    Route::post('notifications/search-notification-agency', 'User\NotificationController@searchNotification')->name('search_notification_user');

    Route::post('read-note', ['before' => 'csrf', 'uses' => 'User\AjaxController@readNote'])->name('readNoteUser');
    Route::post('notifications/read-note', ['before' => 'csrf', 'uses' => 'User\AjaxController@readNote'])->name('readNoteUser');

    Route::get('change/password', 'User\AssessmentController@changepasswordview')->name('change_password');
    Route::post('change/password/submit', 'User\AssessmentController@changePasswordViewSubmit')->name('passwordsubmit');
});
Route::group(['prefix' => 'assessment', 'middleware' => ['user', 'web', 'checkBlocked']], function () {
    Route::get('/{agencyid}/{careid}', 'User\AssessmentController@start')->name('startassessment');
    Route::get('step1/select-patient/{agencyid}/{careid}', 'User\AssessmentController@selectpatientview')->name('step1_select_patient');
    Route::post('userRequestAssessment', 'User\HomeController@userRequestAssessment')->name('userRequestAssessment');
    //begin step1 to sep6
    Route::get('/step1-6/{patientid}/{cid?}', 'User\AssessmentController@step1view')->name('beginStep1')->where(['patientid' => '[0-9]+']);
    Route::post('/step1-submit', 'User\AssessmentController@step1viewsubmit')->name('submitstep1');
    Route::get('/step5-6/{assessid}/{cid?}/{status?}', 'User\AssessmentController@beginStep3Accepted')->name('beginStep4Scheduled');
    Route::post('/decline', 'User\AssessmentController@declineassessment')->name('udeclineaccessment');
    // route to save agency and unsave agency
    Route::get('/save-agency/{aid}/{pid}', 'User\AssessmentController@saveAgencyForPatient')->name('savedagencyforpatient');
    Route::get('/unsave-agency/{aid}/{pid}', 'User\AssessmentController@unSaveAgencyForPatient')->name('unsaveagencyforpatient');
});

Route::get('/save/agencyforpatient/{aid}', 'User\AssessmentController@showPatientListForAgency')->name('saveagencyforpatient');
Route::get('admin/step1-6/{patientid}/{cid?}/{uid?}', 'User\AssessmentController@adminstep1view')->name('adminbeginStep1');
Route::get('/step1-6/{patientid}/{cid?}', 'User\AssessmentController@step1view')->name('beginStep1');
Route::get('/step2-6/{assessid}', 'User\AssessmentController@step2view')->name('beginStep2');
Route::post('/step2-submit', 'User\AssessmentController@step2viewsubmit')->name('submitstep2');
Route::get('/step3-6/{assessid}/{cid?}', 'User\AssessmentController@beginStep3Accepted')->name('beginStep3Accepted');
Route::get('/step6-6/{id}/{status?}', 'User\AssessmentController@laststep')->name('laststep');

/**
 * 
 * For users module only 
 */
Route::get('user/forgot-password', array('uses' => 'User\ForgotController@index'))->name('userForgotPassword');


Route::post('user/forgot-password', array('before' => 'csrf', 'uses' => 'User\ForgotController@store'));


Route::get('user/resend-verification', array('uses' => 'User\SigninController@resendVerificationShow'))->name('userResendVerification');


Route::post('user/resend-verification', array('before' => 'csrf', 'uses' => 'User\SigninController@resendVerificationSubmit'));
Route::get('user/forgot-sent', function () {
    return view('user.registration.forgotsent');
});
Route::get('user/resent-mail', function () {
    return view('user.registration.resentmail');
});


//getCity
Route::get('get/city', 'User\HomeController@autoCitydata')->name('autoHomeCityUrl');

// Password Reset Screen
Route::get('user/reset-password/{id?}/{route?}', array('uses' => 'User\ForgotController@resetPasswordScreen'));

// Reseting Password
Route::post('user/reset-password', array('uses' => 'User\ForgotController@resetPassword'));

/**
 * For Agency module
 */

Route::get('agency/forgot-password', array('uses' => 'Agency\ForgotController@index'))->name('agencyForgotPassword');


Route::post('agency/forgot-password', array('before' => 'csrf', 'uses' => 'Agency\ForgotController@store'));

Route::get('agency/forgot-sent', function () {
    return view('agency.registration.forgotsent');
});


// Password Reset Screen
Route::get('agency/reset-password/{id?}', array('uses' => 'Agency\ForgotController@resetPasswordScreen'));

// Reseting Password
Route::post('agency/reset-password', array('uses' => 'Agency\ForgotController@resetPassword'));

/**
 * For admin module
 */
// Login Page
Route::post('/login', array('as' => 'login', 'uses' => 'Admin\AdminController@login'))->name('adminlogin');

// Logout
Route::get('/logout', array('uses' => 'Auth\LoginController@logout'))->name('logout');
Route::get('/logout-manual', array('uses' => 'Auth\LoginController@logoutManual'))->name('logout-manual');

//signout
Route::get('/signout', array('uses' => 'Auth\LoginController@signout'))->name('signout');

// Password Reset Screen
Route::get('reset-password/{id}', array('uses' => 'Auth\LoginController@resetPasswordScreen'));

// Reseting Password
Route::post('reset-password', array('uses' => 'Auth\LoginController@resetPassword'));

//Location fetch
Route::post('user/get-location', array('before' => 'csrf', 'uses' => 'User\AjaxController@getLocation'))->name('get-location');

//get state
Route::post('get-state', array('before' => 'csrf', 'uses' => 'User\AjaxController@getStatesByCountry'))->name('get-state');
//get city
Route::post('get-city', array('before' => 'csrf', 'uses' => 'User\AjaxController@getCityByState'))->name('get-city');
Route::post('get-city-name', array('before' => 'csrf', 'uses' => 'User\AjaxController@getCityById'))->name('get-city-name');
Route::post('set-assement-link', array('before' => 'csrf', 'uses' => 'Agency\AjaxController@setAsssessmentSession'))->name('set-assessment-link');


Route::get('get-location-from-ip', function () {
    $position = Location::get();
    //$position = Location::get('192.168.1.1');
    $ip = getIp();
    $data = \Location::get('182.73.240.146');
    $valget[] = Cookie::get('postal_code');
    $valget[] = Cookie::get('city_name');
    $valget[] = Cookie::get('state_name');
    // dd($position);
});

Route::get('refresh-csrf', 'Admin\AjaxController@refreshCSRF');
Route::get('auth/check', 'Admin\AjaxController@checkLoggedIn');

Route::get('/map', 'Common\MapController@index')->name('map');
Route::get('export/{type}/{bid?}/{date?}/{search?}', 'Common\CmsController@export')->name('export');

Route::post('/transfer-image', ['before' => 'csrf', 'uses' => 'Common\CroppedImageUploader@uploadImage']);

//Default website ShareCare
Route::get('/share-care/{pid?}', 'Common\ShareCareController@showPatientIntakeView')->name('share_Care_hub');
Route::get('/sharecare/select-patient', 'Common\ShareCareController@shareCareSelectPatient')->name('shareCareSelectPatient');
Route::get('/search-result-sharecare', 'Common\ShareCareController@searchSubmitResultView')->name('share_submit_view');
Route::post('request/accept', 'Common\ShareCareController@requestAccept')->name('request-accept');
Route::post('save-agency-request-common', 'Common\ShareCareController@saveAgencyRequestCommon')->name('saveAgencyRequestCommon');



//hub link
Route::group(['prefix' => 'hub', 'middleware' => 'user'], function () {
    Route::get('/create', 'Common\HubController@hubViewOne')->name('getHubView');

    Route::post('/submit', 'Common\HubController@hubViewSubmit')->name('hub-submit');

    Route::post('/edit', 'Common\HubController@editHubInfo')->name('editHubInfo');

    Route::get('/accept-terms/{hubid}/{type}', 'Common\HubController@hubViewTwo')->name('hubstep2');

    Route::get('/accept-terms-submit/{hubid}', 'Common\HubController@hubViewTwoSubmit')->name('acceptHubTerms');

    Route::get('/addpatienttohub/{hubid}/{type}', 'Common\HubController@hubAddPatientView')->name('patientViewForHub');

    Route::get('/inviteaction/{requestId}', 'Common\HubController@userInviteAction')->name('user_Invite_Action');

    Route::get('/savepatient/{patientid}/{hubid}', 'Common\HubController@saveHubForPatient')->name('hub_Save_Hub_For_Patient');

    Route::post('sent/request', 'Common\HubController@sendrequestToUser')->name('sent-request');

    Route::post('action/member-request', 'Common\HubController@actionOnMemberRequest')->name('hub_action_member_request');
    Route::post('/request-agency', 'Common\HubController@actionOnAgencyRequest')->name('request_agency');

    Route::post('/add-patient-to-hub/', 'Common\HubController@hostAddPatientToHub')->name('add_patient_to_hub');
    Route::post('/remove-patient-from-hub/', 'Common\HubController@removePatientFromHub')->name('remove_patient_from_hub');


    Route::get('/makehost/{hubid}', 'Common\HubController@becomeHost')->name('make_user_host');

    Route::post('/check-agency-exist/', 'Common\HubController@checkAgencyExist')->name('check_Agency_Exist');

    Route::get('/leave/{hubid}', 'Common\HubController@leaveHost')->name('host_leave_hub');
    Route::get('/memberleave/{hubmemberid}', 'Common\HubController@memberLeaveHost')->name('member_leave_hub');
});


Route::get('/hub-details/{hubid}', 'Common\ShareCareController@showHubDeatils')->name('showhubview');
Route::get('/hub-details/{hubid}/select-patient', 'Common\ShareCareController@selectpatientview')->name('hubSelectPatient');

Route::get('/get-currency', 'User\HomeController@getCurrency')->name('get-currency');
Route::post('/set-timezone', ['before' => 'csrf', 'uses' => 'Admin\AjaxController@setTimeZone']);
Route::post('/get-avg-care-price', ['before' => 'csrf', 'uses' => 'Admin\AjaxController@getCityAvgPrice'])->name('getCityAvgPrice');

// cms urls
Route::group(['prefix' => 'content'], function () {
    Route::get('/contact-us', 'Common\CmsController@contactUs')->name('contactUs');
    Route::post('/contact-us', 'Common\CmsController@contactUsSubmit')->name('contactUsSubmit');

    Route::get('/help', 'Common\CmsController@help')->name('help');
    Route::get('/agencyfaq', 'Common\CmsController@agencyfaq')->name('agencyfaq');
    Route::get('/faq', 'Common\CmsController@faq')->name('faq');
    Route::get('/aboutus', 'Common\CmsController@aboutus')->name('aboutus');
    Route::get('/coronavirus', 'Common\CmsController@corona')->name('corona');
    Route::get('/{homecare}', 'Common\CmsController@cms')->name('homecare');
    Route::get('/{company}', 'Common\CmsController@cms')->name('company');
    Route::get('/{sitemap}', 'Common\CmsController@cms')->name('sitemap');
    Route::get('/{termsofservice}', 'Common\CmsController@cms')->name('termsofservicess');
    Route::get('/{privacypolicy}', 'Common\CmsController@cms')->name('privacypolicy');
    Route::get('/{agencydetail}', 'Common\CmsController@cms')->name('agencydetail');
    Route::get('/{learnmore}', 'Common\CmsController@cms')->name('learnmore');
});

Route::get('/city_drop_list', 'Common\CmsController@cityDropList')->name('city_drop_list');
Route::post('/get-cities', 'Common\CmsController@getCities')->name('get-cities');

// Blog/wink
Route::get('/blog', 'BlogController@index')->name('blog.index');
Route::get('/blog/{slug}', 'BlogController@show')->name('blow.show');


Route::get('/facility', 'LTCController@index')->name('ltc.index');
Route::post('/ltc-request', 'LTCController@post')->name('ltc.post');

//Food delivery
Route::get('/communitysupport', 'CommunitySupportController@index')->name('communitysupport.index');

//Care Navigator 
Route::get('/carenavigator', 'CareNavigatorController@index')->name('carenavigator.index');
// New Sharecare
Route::get('/sharecare/demo', 'Common\ShareCareController@index')->name('shareHome');
// Learn more about Sharecare
Route::get('/sharecare/learn-more', 'Common\ShareCareController@showLearnMore')->name('sharecare_learn_more');
// Add Patient Information
Route::get('hub-details/add-patient-info/{hubid?}/{pid?}', 'Common\ShareCareController@sharecareAddPatientInfo')->name('sharecareAddPatientInfo');
Route::post('hub-details/add-patient-info-submit', 'Common\ShareCareController@sharecareAddPatientInfoSubmit')->name('sharecareAddPatientInfoSubmit');
// Add Hub request information after signup
Route::get('/addhubrequest/{pid}/{hubid}', 'Common\ShareCareController@addhubrequest')->name('addhubrequest');
Route::post('/addhubrequestsubmit', 'Common\ShareCareController@addHubRequestSubmit')->name('addHubRequestSubmit');
Route::post('/posthubrequest', 'Common\ShareCareController@posthubrequest')->name('posthubrequest');
// Admin ShareCare 
Route::get('admindashboard/sharecare-hubs', 'Admin\ShareCareController@viewAdminShareCareHubs')->name('viewAdminShareCareHubs');
Route::get('sharecare/requests', 'Admin\ShareCareController@viewAdminShareCareRequests')->name('viewAdminShareCareRequests');
Route::get('sharecare/matches', 'Admin\ShareCareController@viewAdminShareCareMatches')->name('viewAdminShareCareMatches');
Route::get('sharecare/details/{pid}', 'Admin\ShareCareController@viewAdminShareCareDetails')->name('viewAdminShareCareDetails');
Route::get('sharecare/settings', 'Admin\ShareCareController@adminShareCareSettings')->name('adminShareCareSettings');
Route::post('sharecare/details/update', 'Admin\ShareCareController@updateShareCareInfo')->name('adminUpdateShareCareInfo');
Route::post('/admin/addhubrequestsubmit', 'Admin\ShareCareController@addHubRequestSubmit')->name('adminAddHubRequestSubmit');
Route::post('sharecare/settings/update', 'Admin\ShareCareController@updateAdminShareCareSettings')->name('updateAdminShareCareSettings');
Route::post('hubs/details/update-hub-price', 'Admin\ShareCareController@updateHubPrice')->name('adminUpdateHubPrice');
// Create Hub
Route::post('/hub-submit', 'Admin\HubsController@createSubmit')->name('hub-create');
Route::get('/hubs/details/{hid}', 'Admin\UserController@HubDetail')->name('hub-detail');
Route::get('/hubs/members/{hid}', 'Admin\UserController@HubMembers')->name('hub-members');
Route::get('/users/patients/hubs/find/{uid}/{pid}', 'Admin\UserController@findHub')->name('find-hub');
// Add Member to Hub
Route::get('/add-hub-member/{hid}/{pid}', 'Admin\UserController@addMember')->name('add_member');
Route::post('/add-hub-member', 'Admin\UserController@hostAddPatientToHub')->name('add_hub_member');
Route::post('/remove-hub-member', 'Admin\UserController@removePatientFromHub')->name('remove_hub_member');
// Choose member
Route::get('/choose-patient/{hid}', 'Admin\PatientController@choose')->name('choose-patient');
Route::get('/admin/accept-terms/{hubid}/{type}', 'Admin\UserController@hubViewTwo')->name('adminhubstep2');

// Sharecare FAQ Page
Route::get('/sharecarefaq', 'Common\ShareCareController@sharecarefaq')->name('sharecarefaq');

// Temporary page - ShareCare Hub browse
Route::get('/sharecare-hubs', 'Common\ShareCareController@sharecareHubsBrowse')->name('sharecare_hubs_browse');


// Senior Chatline
Route::get('/seniorchatline', 'SeniorChatlineController@index')->name('chatline');
Route::post('/seniorchatline', 'SeniorChatlineController@requestCall')->name('chatline_request');

// ShareCare patient intake form
Route::get('/sharecare/{pid?}', 'Common\ShareCareController@showPatientIntakeView')->name('share_Care_hub');


//Route to render VueJS - route will show vueapp blade 
//the VueJS component name is what will show in the URL
 Route::get('/{any}', function(){
            return view('vueapp');
    })->where('any', '.*');