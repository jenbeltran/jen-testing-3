@extends('layout.master_patient_dashboard')

@section('css')
<style>
#cropper_modal .modal-lg {
    max-width: 1000px;
}


.webcam_cropped_image_preview {
    overflow: hidden;
}

.cropped_image_preview {
    overflow: hidden;
}

.text-danger {}

[data-id="city"] {
    display: none;
    overflow: hidden;
}
[data-id="province"] {
    display: none;
    overflow: hidden;
}
[data-id="country"] {
    display: none;
    overflow: hidden;
}

</style>
<link rel="stylesheet" type="text/css" href="{{ asset('assets\css\plugin\jquery.datetextentry.css') }}">
<link href="{{ asset('/cropperAssets/cropper.min.css') }}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="{{ asset('assets/css/pages/dash-user_patient.css') }}">
@endsection @section('content')
@php
$latitude='';
$longitude='';
$ip = getIp();
$data = \Location::get($ip);
if($data){
    $latitude=$data->latitude;
    $longitude=$data->longitude;
}
if(isset($patientData->latitude)){
   $latitude=$patientData->latitude;
   $longitude=$patientData->longitude;
}
$patientProfilePic='';
if(isset($patientData->patient_profile_pic)){
    $patientProfilePic=trim($patientData->patient_profile_pic);
}

@endphp

<div class="page-container">
   
<div class="inr_wrap">

    @include('template.dash_sidebar',['patient'=>$patient,'loginuser'=>$loginuser,'urlType'=>config('constants.URLS.ADDPVIEW')])
    <div class="main-content">
        @include('template.dash_step_header', ['type' => 1,'urlType'=>config('constants.URLS.ADDPVIEW')])
        <div class="container">
            @if(Session::has('message'))
            <div class="alert {{ Session::get('alert-class', 'alert-success') }}">
                {{ Session::get('message') }}
            </div>
            {{Session::forget('message')}}
            @endif {!! Form::open(['route' => 'addpviewsubmit','method' => 'POST','files'=>'true','id'=>'patient_profile_form']) !!}
        <input name='backUrl' value="{{$backUrl}}"  hidden />
        <input name='key' value="{{$key}}"  hidden />
        <input name='parameters' value="{{$parameters}}"  hidden />


          <div class="newform">
                <div class="row more-space">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="" class="control-label">Upload/Change Image</label>
                            <div class="input-group image-uploader">
                                <input type="text" id="inputImage" class="image-preview-filename form-control" value="{{(!empty(trim($patientProfilePic)))?$patientProfilePic:( (isset($patientData->is_user) && $patientData->is_user==YES && !empty(trim($loginuser->user_profile_pic)))? asset(S3_IMAGE_PATH.$loginuser->user_profile_pic):'') }}" name="user_image" id="user_image" readonly="readonly">
                                <!-- image-preview -->
                                <div id="cropped_image_preview1" class="popover fade bottom profile-admin-img img-responsive img-circle">
                                    <div class="arrow" style="left: 50%;"></div>
                                    <h3 class="popover-title">Preview <span class="close"></span></h3>
                                    <div class="popover-content">
                                        <span id="cropped_image_preview" style="background: url({{ (!empty($patientProfilePic))?S3_IMAGE_PATH .$patientProfilePic:'/assets/images/placeholder_190.png'}});display: block;
                    height: 190px;
                    background-size: 190px;
                    background-position: center;
                    background-repeat: no-repeat;
                    border:1px solid #f1f1f1;
                    width: 190px;">
</span>
</div>
</div>
<!-- image-preview -->

<span class="input-group-btn">
 <div class="btn btn-border success image-preview-input">
  <span class="image-preview-input-title"> &nbsp; Browse</span>
  <input id="image-selector" type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"  @if(isset($patientData->is_user) && $patientData->is_user) disabled @endif />
  <!-- rename it -->
</div>
</span>
</div>
<input type="text" value="{{(!empty($patientProfilePic))?$patientProfilePic:''}}" name="userImage" required="" id="userImage" hidden> @if ($errors->has('userImage'))
<span class="error">{{ $errors->first('userImage') }}</span> @endif
</div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        <div class="divlabel">
            <span class="control-label">Email</span>
            <div class="custom-checkbox">
                @if (isset($patient->is_primary) and $patient->is_primary==1)
                    <input type="checkbox" checked id="same_as_pri" name="same_as_pri">
                 @else
                    @if(isset($patientData->is_user) && $patientData->is_user) 
                     <input type="checkbox" id="same_as_pri" name="same_as_pri" checked disabled> 
                    @else 
                        <input type="checkbox" id="same_as_pri" name="same_as_pri" @if(isset($patientData->patient_email) && $patientData->patient_email == $user->user_email || isset($patientData->patient_email) && !$patientData->patient_email || !isset($patientData->patient_email)) checked @endif>
                    @endif

                @endif
                <label for="same_as_pri">Same as primary account</label>
            </div>

        </div>
        <input type="email" id="patient_email" class="form-control removespace" maxlength="40"  name="patient_email" onblur="a = this.value;this.value = a.trim()" autocomplete="off"  value="@if(old('patient_email')){{old('patient_email')}}@else{{ (isset($patientData->patient_email) && $patientData->patient_email)?$patientData->patient_email:((isset($patientData->is_user) && $patientData->is_user)?Auth::user()->email:(isset($user->user_email)?$user->user_email:''))}}@endif"  @if(isset($patientData->is_user) && $patientData->is_user==YES || !isset($patientData->patient_email) || !$patientData->patient_email)) readonly @endif /> @if ($errors->has('patient_email'))       
         <span class="error">{{ $errors->first('patient_email') }}</span> @endif
    </div>

    <input id="useremail" value="{{isset($user->user_email)?$user->user_email:''}}" name="useremail" hidden>
    <input id="patientemail" value="{{isset($patientData->patient_email)?($patientData->patient_email ? $patientData->patient_email:(isset($user->user_email)?$user->user_email:'')):(isset($user->user_email)?$user->user_email:'')}}" name="patientemail" hidden>

</div>

<div class="col-sm-6">

    <div class="form-group">
        <div class="row">
            <div class="col-xs-6">
                <label class="control-label required">First Name</label>
                <input type="text" id="firstName" class="form-control removespace" maxlength="50"  name="firstName" onblur="a = this.value;this.value = a.trim()" autocomplete="nope" required="" value="@if(old('firstName')){{old('firstName')}}@else{{ isset($patientData->first_name)?$patientData->first_name:''}}@endif" @if(isset($patientData->is_user) && $patientData->is_user) readonly @endif />
                 @if ($errors->has('firstName'))
                <span class="error">{{ $errors->first('firstName') }}</span>
                 @endif
            </div>
            <div class="col-xs-6">
                <label class="control-label required">Last Name</label>
                <input type="text" id="lastName" class="form-control removespace" maxlength="50" name="lastName" onblur="a = this.value;this.value = a.trim()" autocomplete="nope" required="" value="@if(old('lastName')){{old('lastName')}}@else{{ isset($patientData->last_name)?$patientData->last_name:''}}@endif" @if(isset($patientData->is_user) && $patientData->is_user) readonly @endif/>
                 @if ($errors->has('lastName'))
                <span class="error">{{ $errors->first('lastName') }}</span> 
                @endif
            </div>

        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-xs-6">
                <label class="control-label required">Date of Birth </label>
                <p id="customError" hidden>Please input a valid Date of Birth.</p>
                <input name="patient_dob" autocomplete="off" id="patient_dob" type="text" class="form-control" required="" value="@if(old('patient_dob')){{old('patient_dob')}}@else{{ isset($patientData->patient_dob)?date('Y-m-d',strtotime($patientData->patient_dob)):''}}@endif" onblur="a = this.value;this.value = a.trim()" > @if ($errors->has('patient_dob'))
                <span class="error">{{ $errors->first('patient_dob') }}</span> @endif
            </div>
            <div class="col-xs-6">
                <label class="control-label">Phone Number </label>
                <input name="patient_phone" autocomplete="off" id="patient_phone" maxlength="12" type="tel" placeholder="555-555-5555" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"  onblur="a = this.value;this.value = a.trim()" class="form-control" value="@if(old('patient_phone')){{old('patient_phone')}}@else{{ isset($patientData->patient_phone)?$patientData->patient_phone:''}}@endif"> @if ($errors->has('patient_phone'))
                <span class="error">{{ $errors->first('patient_phone') }}</span> @endif
            </div>
        </div>
    </div>
    <div class="form-group">
        <input type="hidden" name="updateid" value="{{base64encryption($patientid)}}">
        <div class="form-group">
            <label class="control-label required">Patient's Relationship to You</label>
            <input type="hidden" value="{{isset($patient['city_id'])?$patient['city_id']:''}}"  >
            <select name="relation" id="relation" required="">
              <option value="">Select Relationship</option>
              @if($relationship->count() > 0)
              @foreach($relationship as $relation)
              <option value="{{isset($relation->id)?$relation->id:''}}" @if(isset($patientData->relationship_id) and ($relation->id == $patientData->relationship_id) or old('relation') == $relation->id) {{'selected'}} @else  @if(isset($patientData->is_user) && $patientData->is_user) disabled @endif @endif>{{$relation->title}}  </option>
              @endForeach
              @else
              No Record Found
              @endif
          </select> 
          @if ($errors->has('relation'))
          <span class="error">{{ $errors->first('relation') }}</span> 
          @endif
      </div>

  </div>

</div>

<div class="col-sm-6">
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label class="control-label required">Address Line 1</label>
                <input name="patient_address" required="" autocomplete="nope" id="eventLocation" class="form-control"  value="@if(old('patient_address')){{old('patient_address')}}@else{{ isset($patientData->address)?$patientData->address:''}}@endif" onfocus="this.setAttribute('autocomplete', 'nope');">
                <p id="customError" hidden>Please select a valid address from the dropdown.</p>
                <!--<button style="float:right;display:none" class="btn btn-primary" type="button" data-toggle="modal" data-target="#myModal" id="setLocation" type="button">Set Location</button>-->
                <input type="hidden" id="autoSelectedAddress" value="">
                @if ($errors->has('patient_address'))
                <span class="error">{{ $errors->first('patient_address') }}</span> @endif
                
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label">Address Line 2 {{--(Apartment, suite, unit, building, floor, etc.)--}}</label>
                <input name="patient_address2"  placeholder="Apartment, suite, unit, etc." autocomplete="nope" id="eventLocation2" class="form-control"  value="@if(old('patient_address2')){{old('patient_address2')}}@else{{ isset($patientData->address_two)?$patientData->address_two:''}}@endif">
                <!--<button style="float:right;display:none" class="btn btn-primary" type="button" data-toggle="modal" data-target="#myModal" id="setLocation" type="button">Set Location</button>-->
                <input type="hidden" id="autoSelectedAddress" value="">
                @if ($errors->has('patient_address2'))
                <span class="error">{{ $errors->first('patient_address2') }}</span> @endif
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label">Postal Code</label>
                <input type="text" name="postal_code" maxlength="6" id="postal_code" autocomplete="nope" class="form-control"  onblur="a = this.value;this.value = a.trim()" value="@if(old('postal_code')){{old('postal_code')}}@else{{ isset($patientData->postal_code)?$patientData->postal_code:''}}@endif" autocomplete="off"> @if ($errors->has('postal_code'))
                <span class="error">{{ $errors->first('postal_code') }}</span> @endif
            </div>
        <div class="form-group">
            <input type="hidden" value="{{isset($patientData->city_id)?$patientData->city_id:''}}" name='ccity' id="ipcity">
            {{-- <label class="control-label ">City</label> --}}
            <select name="city" id="city" data-live-search="true" onchange='updateLatLong(this.value);'>
            @if($citydata)

                @forelse($citydata as $city)
                @php
                $selected='';
                if($patientData->city_id==$city->id){
                    $selected='selected';
                }
                @endphp
                    <option {{$selected}} value='{{$city->id}}'>{{$city->name}}</option>
                @empty
                @endforelse
            @endif
            </select> @if ($errors->has('city'))
            <span class="error">{{ $errors->first('city') }}</span> @endif
 </div>


    </div>
    <div class="row">
        <div class="col-sm-0">
            <div class="form-group">
                <input type="hidden" value="{{isset($patientData->province_id)?$patientData->province_id:''}}" name='pprovince' id="pprovince">
                {{-- <label class="control-label ">Provinces</label> --}}
                <select name="province" id="province" onchange="getCities(this.value,'city');" data-live-search="true" >
                @if($statedata)

                 @forelse($statedata as $province)
                 @php
                 $selected='';
                   if($patientData->province_id==$province->state_code){
                       $selected='selected';
                   }
                 @endphp
                     <option {{$selected}} value='{{$province->state_code}}'>{{$province->name}}</option>
                 @empty
                 @endforelse
               @endif
                </select>
                  @if ($errors->has('province'))
             <span class="error">{{ $errors->first('province') }}</span> @endif
         </div>
     </div>
     {{-- <div class="col-sm-0">
            <div class="form-group">
                <label class="control-label">Postal Code</label>
                <input type="text" name="postal_code" maxlength="6" id="postal_code" class="form-control"  onblur="a = this.value;this.value = a.trim()" value="@if(old('postal_code')){{old('postal_code')}}@else{{ isset($patientData->postal_code)?$patientData->postal_code:''}}@endif" autocomplete="off"> @if ($errors->has('postal_code'))
                <span class="error">{{ $errors->first('postal_code') }}</span> @endif
            </div>
        </div> --}}



 <div class="col-xs-0">
    <input type="hidden" value="{{isset($patientData->country_id)?$patientData->country_id:''}}" name='ccountry' id="ccountry">
    {{-- <label class="control-label ">Country</label> --}}

    <select name="country" id="country" onchange="getStates(this.value,'province');" data-live-search="true">
     <option value="">Select Country</option>
     @forelse($country as $key=>$val)
     <option @isset($patientData->country_id) @if($val['country_code1']==$patientData->country_id) selected="" @endif @endisset value="{{$val['country_code1']}}">{{$val['name']}}</option>
     @empty
     <option value="">No Record</option>
     @endforelse
 </select>
  @if ($errors->has('country'))
 <span class="error">{{ $errors->first('country') }}</span> @endif
</div>

</div>

</div>

<div class="col-sm-6">

</div>
<!-- Map integration started from here  -->

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
<div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Map</h4>
    </div>
    <div class="modal-body">
        <!-- Map Content Here -->

            <div class="double clearfix">
            <div style="display:none" class="row" >
            <label>Latitude </label>
            <div class="input">
                <input type="text" name="latitude" id="latitude" readonly/>
            </div>
            <label class="error_div"></label>
            </div>
            <div style="display:none"  class="row">
            <label>Longitude</label>
            <div class="input">
                <input type="text" name="longitude" id="longitude" readonly/>
            </div>
            <label class="error_div"></label>
            </div>
            <input  type="hidden" name="locality1" id="locality1" readonly/>
            <input type="hidden" name="sublocality_level_1" id="sublocality_level_1" readonly/>

            <input type="hidden" name="route" id="route" readonly/>
            <input type="hidden" name="city1" id="city1" readonly/>
            <input type="hidden" name="state1" id="state1" readonly/>
            <input type="hidden" name="country1" id="country1" readonly/>
            <input type="hidden" name="postalcode1" id="postalcode1" readonly/>
            <input type="hidden" value="" name="hubPostalCode" id="hubPostalCode" class="form-control"  required="">
            <div class="form-group">
                <label>Event Location</label>

                    <input type="text" class="form-control" name="Location_2" id="Location_2"/>

                <label class="error_div"></label>
            </div>
            <div class="form-group">
                <label class="">Select Location</label>
                <div class="mapLA wel" id="mapLA" style="width: 100%; padding-top:10px;">
                    <div id="map" style="width: 100%; height: 400px"></div>
                    <div id="markerStatus" onload="initialize()"></div>
                    <div style="display:none" id="info"></div>
                    <div style="display:none" id="address1"></div>
                </div>
            </div>
        </div>

        <!--- Map Content End Here -->

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
    </div>

</div>
</div>
<!-- Map integration ended here -->

</div>

<div class="form-btns text-right">
    <input type="submit" id="buttonUpdate" name="Save" value="{{isset($patientData) ? 'Update' :'Save'}}" class="btn btn-success btn-effect">
</div>
</div>

</div>
@include('template.dash_footer')
</div>
{{ Form::close() }}

</div>
</div>
@endsection

@section('js_cropper_validate')

<script src="{{asset('assets/js/pages/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/js/pages/validation.js')}}"></script>
<script src="{{asset('assets/js/pages/global-msg.js')}}"></script>
<script src="{{asset('assets/js/pages/common-validation.js')}}"></script>
<!-- required for validation -->

<script src="{{ asset('/cropperAssets/cropper.js') }}" type="text/javascript"></script>
<script src="{{asset('/cropperAssets/appinventivCropper.js')}}" type="text/javascript"></script>
@endsection
@section('loader_script')
    <script type="text/javascript">
    var get_state_url = "{{route('get-state')}}";
    var get_city_url = "{{route('get-city')}}";
    var get_city_name_url = "{{route('get-city-name')}}";
    </script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places,drawing&key=<?php echo GOOGLE_API_KEY ?>"></script>
  
    <script src="{{asset('/assets/js/pages/jquery.tokeninput.js')}}"></script>

    <script src="{{asset('js/lib/googlemap2/mymap.js')}}"></script>
    <script src="{{asset('/assets/js/pages/country-list.js')}}"></script>
    <script type="text/javascript">
    var latlng = new google.maps.LatLng('<?php echo $latitude ?>','<?php echo $longitude ?>');

    $(function (){
        initialize();
    })
    </script>

    <script src="{{ asset('assets/js/pages/dash-user_patient.js') }}"></script>
    <script src="{{ asset('assets/js/pages/location-service.js') }}"></script>
    <script src="{{asset('assets/js/pages/country-list.js')}}"></script>
    {{-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> --}}
    <script type="text/javascript" src="{{ asset('assets\js\plugin\jquery.datetextentry.js') }}"></script>
@endsection
