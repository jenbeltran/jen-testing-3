import Vue from 'vue'
import VueRouter from 'vue-router'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

Vue.use(Buefy)
Vue.use(VueRouter)

//Page Layout in vuapp blade
import Navbar from './components/Layout/Navbar'
import App from './components/Layout/App'
import HomeFooter from './components/Layout/HomeFooter'

//App content pages
import Welcome from './components/HomePage/Welcome'
import LandingPage from './components/LandingPage/LandingPage'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/landing-page',
            name: 'landing-page',
            component: LandingPage,
        },
        {
            path: '/home',
            name: 'home',
            component: Welcome,
        },
        {
            path: '/homecarehub',
            name: 'homecarehub',
            beforeEnter() {location.href = 'https://www.homecarehub.com/'}
        }
    ],
})

const app = new Vue({
    el: '#app',
    components: { Navbar, App, HomeFooter },
    router,
});