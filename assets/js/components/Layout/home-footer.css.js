import styled from "vue-styled-components"
import { colors } from "../constants/colors"
import { breakpoints } from "../constants/breakpoints"

const { tablet } = breakpoints
const { darkGrey, grey } = colors

export const StyledFooter = styled.footer`
  font-size: 14px;
`

export const ColumnSection = styled.div`
  display: flex;
  flex-direction: column;
  flex-shrink: 0;
  padding: 1rem;
  color: ${grey};

  @media screen and (min-width: ${tablet}px) {
    justify-content: space-around;
    align-items: start;
    flex-direction: row;
  }
`
export const IndividualColumn = styled.div`
  width: 20%
  padding: 2rem;
`

export const FooterLink = styled.a`
  text-decoration: underline;
  color: ${darkGrey};
  cursor: pointer;
`

export const UL = styled.ul`
  padding: 0;
  margin: 0;
`

export const LI = styled.li`
  color: ${darkGrey}
`
export const SocialIconList = styled(UL)`
  display: flex;
  justify-content: center;

  @media screen and (min-width: ${tablet}px) {
    justify-content: flex-start;
  }
`

export const NavModalParagraph = styled.p`
  text-decoration: underline;
  cursor: pointer;
`

export const SocialParagraph = styled.p`
  margin-top: 0;
  text-align: center;

  @media screen and (min-width: ${tablet}px) {
    text-align: left;
  }
`
