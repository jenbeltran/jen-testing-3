import styled from "vue-styled-components"
import { colors } from "./colors"

const { fontGrey } = colors

export const Header1 = styled.h1`
  font-weight: 600;
  color: ${fontGrey};
  font-size: 48px;
  line-height: normal;
`
export const Header2 = styled.h2`
  font-weight: 600;
  color: ${fontGrey};
  font-size: 36px;
  line-height: normal;
`
export const Heading2 = styled.h2`
  font-weight: 600;
  color: ${fontGrey};
  font-size: 24px;
  line-height: normal;
`
export const Heading3 = styled.h3`
  font-weight: 600;
  color: ${fontGrey};
  font-size: 20px;
  line-height: normal;
`
export const Paragraph = styled.p`
  color: ${fontGrey};
  font-size: 18px;
  line-height: normal;
`
export const ParagraphSmall = styled.p`
  color: ${fontGrey};
  font-size: 14px;
  line-height: normal;
`