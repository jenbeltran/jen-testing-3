export const colors = {
  blue: "#1589F9",
  skyBlue: "#2FC4FF",
  navyBlue: "#38465B",
  green: "#25C387",
  whiteGrey: "#EEEEEE",
  lightGrey: "#CCCCCC",
  grey: "#93A1B5",
  darkGrey: "#56657D",
  fontGrey: "#617088"
}
