export const breakpoints = {
  phone: 480,
  tablet: 770,
  desktop: 1200,
}