 <!doctype html>
    <html lang="{{ app()->getLocale() }}">
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <title>HomecareHub</title>
            {{-- Google Font Lato --}}
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap">
            {{-- Buefy VueJS Library --}}
            <link rel="stylesheet" href="https://cdn.materialdesignicons.com/5.3.45/css/materialdesignicons.min.css">
        </head>

        <body>
            <div id="app">
                <navbar></navbar>
                <app></app>
                <home-footer></home-footer>
            </div>
            <script type="text/javascript" src="{{ mix('js/vueapp.js') }}"></script>
        </body>

    </html>