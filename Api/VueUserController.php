<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;


class VueUserController extends Controller
{
    public function index(){
          return (new Response(
           User::orderBy('created_at', 'desc')->take(5)->get()
        , 200));
    }

    public function store(Request $request){
        //  $user = new User();
        //  $user->first_name = request("first_name");
        //  $user->last_name = request("last_name");
        //  $user->user_email = request("user_email");
        //  $user->save();

        return (new Response(
            $request->all()
        , 201));
    }

    
}
